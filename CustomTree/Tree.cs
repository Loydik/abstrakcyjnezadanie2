﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CustomTree
{
    public class Tree<T> : IEnumerable<T>
    {
        #region Private Methods

        private EnumeratorOrder _order;

        #endregion


        #region Public Properties

        public T Value;
        public LinkedList<Tree<T>> Children { get; }

        public EnumeratorOrder Order
        {
            get { return _order; }
            set
            {
                if (!Enum.IsDefined(typeof (EnumeratorOrder), value))
                {
                    throw new ArgumentOutOfRangeException();
                }

                UpdateChildrenOrder(value);
                _order = value;

            }
        }

        #endregion


        #region Construction

        public Tree(T value, EnumeratorOrder order)
        {
            Value = value;
            Order = order;
            Children = new LinkedList<Tree<T>>();
        }

        public Tree(T value, EnumeratorOrder order, IEnumerable<T> children)
        {
            Value = value;
            Order = order;
            Children = (LinkedList<Tree<T>>) children;
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Gets Traversal algorithm based on the current Order property
        /// </summary>
        /// <returns></returns>
        private IEnumerable<T> GetTraversalAlgorithm(Tree<T> root)
        {
            if (Order == EnumeratorOrder.BreadthFirstSearch)
            {
                return BreadthFirstTopDownTraversal(root);
            }

            return DepthFirstTopDownTraversal(root);
        }

        /// <summary>
        /// Updates children order, when the parent order is changed
        /// </summary>
        /// <param name="order">EnumeratorOrder</param>
        private void UpdateChildrenOrder(EnumeratorOrder order)
        {
            if (Children != null)
            {
                foreach (var child in Children)
                {
                    child.Order = order;
                }
            }
        }

        #endregion


        #region Public Methods

        public void Add(Tree<T> child)
        {
            child.Order = Order;

            foreach (var element in child.Children)
            {
                element.Order = Order;
            }

            Children.AddLast(child);
        }

        public void Add(T child)
        {
            var newTree = new Tree<T>(child, Order);

            foreach (var element in newTree.Children)
            {
                element.Order = Order;
            }

            Children.AddLast(newTree);
        }

        /// <summary>
        /// Algorithm for breadth-traversing the tree 
        /// </summary>
        /// <param name="root">The root element</param>
        /// <returns></returns>
        public static IEnumerable<T> BreadthFirstTopDownTraversal(Tree<T> root)
        {
            Queue<Tree<T>> queue = new Queue<Tree<T>>();
            queue.Enqueue(root);

            while (queue.Any())
            {
                var currentNode = queue.Dequeue();

                if (currentNode == null)
                {
                    break;
                }

                yield return currentNode.Value;

                foreach (var child in currentNode.Children)
                {
                    queue.Enqueue(child);
                }
            }
        }

        /// <summary>
        /// Algorithm for depth-traversing the tree 
        /// </summary>
        /// <param name="root">The root element</param>
        /// <returns></returns>
        public static IEnumerable<T> DepthFirstTopDownTraversal(Tree<T> root)
        {
            Stack<Tree<T>> stack = new Stack<Tree<T>>();
            stack.Push(root);

            while (stack.Any())
            {
                var currentNode = stack.Pop();

                if (currentNode == null)
                {
                    break;
                }

                yield return currentNode.Value;

                foreach (var child in currentNode.Children.Reverse()) // Reverse needed so the tree will be traversed from left to right
                {
                    stack.Push(child);
                }
            }
        }

        public IEnumerable<Tree<T>> GetChildren()
        {
            return Children;
        }

        #endregion


        #region IEnumerable Members 

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var element in GetTraversalAlgorithm(this))
            {
                yield return element;
            }
            
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

    }
}
